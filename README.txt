Overview:
--------
Counter service integrates sharrre.com library for displaying customizable
social media counter, into a service to be used with Services Link module.
(https://www.drupal.org/project/service_links)

Compare to the widgets (Facebook, Twitter...) existing in the sub-module
Widget Services (using iframes), "with Sharrre you can create uniform buttons to
integrate with your designs".
API request to the social media websites and markup complexity are minimum,
improving loading of the page.
By default counter_link provide display font-icon each social media, it is easy
to customize by css.

However for the services Google+ and Stumbleupon, as indicated by the
sharrre.com author, there is no proper API to retreive the count information.
This is done by "reverse engineering" of the response sent by those services.
Therefore it is not officially supported.

* Sharrre - http://sharrre.com/



Installation:
------------
  1. Download the Sharrre library (Tested with 29/07/2014 version
  (https://github.com/Julienh/Sharrre/archive/1da97157b43768c8e87fdf56b5dc0420b0c60ec3.zip)
  and extract the file under sites/all/libraries.
  2. Download and enable the module.
  3. Visit Service Links admin page (admin/config/services/service-links/services)
  4. check the box of the "counter links" you want to display.
  5. make sure that that you are showing the service links block on the page (or
  display the links IN the content in service_links admin page)

