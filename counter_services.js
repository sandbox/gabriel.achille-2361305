(function ($) {
  Drupal.behaviors.counter_services = {
    attach: function (context, settings) {
      // class service-links-XXX-counter is being added automatically by service_links
      $('.service-links-facebook-counter div.sharrre-element', context).each(function(){
        //$(this).append('<span class="counter">REAL C</span>');
        var counter_service = 'facebook';
        counter_services_sharrre(counter_service, $(this));
      });

      $('.service-links-twitter-counter div.sharrre-element', context).each(function(){
        var counter_service = 'twitter';
        counter_services_sharrre(counter_service, $(this));
      });

      $('.service-links-googleplus-counter div.sharrre-element', context).each(function(){
        //$(this).append('<span class="counter">REAL C</span>');
        var counter_service = 'googlePlus';
        counter_services_sharrre(counter_service, $(this));
      });

      $('.service-links-linkedin-counter div.sharrre-element', context).each(function(){
        var counter_service = 'linkedin';
        counter_services_sharrre(counter_service, $(this));
      });

    }
  }

  counter_services_sharrre = function(counter_service, element) {
    // build a temp object because we one of its key is a variable:
    var object_share = {};
    object_share[counter_service] = true;

    $(element).sharrre({
      share: object_share,
      // make a themeable and accessible template:
      template: '<span class="service-icon"></span><span class="service-count"><span class="element-invisible">Shared </span>{total}<span class="element-invisible"> times</span></span>',
      enableHover: false,
      enableTracking: true,
      // SharrreUrlCurl is only defined by drupal backend if using the googleplus counter.
      urlCurl: typeof Drupal.settings.counter_services.SharrreUrlCurl != 'undefined' ? Drupal.settings.counter_services.SharrreUrlCurl : 'sharrre.php',
      click: function(api, options){
        api.simulateClick();
        $(this).parent().parent().click();
        api.openPopup(counter_service);
        // TODO: stop propagation of event to parent (because we have an popup opening AND current windows going to the social media website...)
      }
    });
  }

})(jQuery);
